%define EXIT 60
%define STDOUT 1
%define NEW_STRING 0xA
%define SPACE 0x20
%define TAB 0x9
%define MIN 0x30
%define MAX 0x39


section .text
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax,EXIT
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    .loop:
        cmp byte[rdi + rax], 0
        je .return
        inc rax
        jmp .loop
    .return:
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push r12
    push r13
    mov r12, rdi
    xor r13, r13

    .loop:
        cmp byte[r12 + r13], 0
        je .return
        mov rdi, [r12 + r13]
        call print_char
        inc r13
        jmp .loop

    .return:
        pop r13
        pop r12
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rax, STDOUT
    mov rdi, STDOUT
    mov rsi, rsp
    mov rdx, STDOUT
    syscall
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, NEW_STRING
    jmp print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    push r12
    push r13
    push r14
    sub rsp, 20
    mov r12, rsp
    xor r13, r13
    mov rax, rdi
    mov r8, 10
    
    .loop:
        xor rdx, rdx
        div r8
        add dl, MIN
        lea r14, [r12 + r13]
        mov byte[r14], dl
        inc r13
        test rax, rax
        jz .print
        jmp .loop

    .print:
    	dec r13
        cmp r13, 0
        jl .stop
        xor rdi, rdi
        lea r14, [r12 + r13]
        mov dil, byte[r14]
        call print_char
        jmp .print
       
    .stop:
        add rsp, 20
        pop r14
        pop r13
        pop r12
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    test rdi, rdi
    jns print_uint
    mov r9, rdi
    mov rdi, '-'
    call print_char
    mov rdi, r9
    neg rdi
    jmp print_uint

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rcx, rcx				; текущий индекс символа
    xor rax, rax
    
    .loop:
        mov al, byte[rdi + rcx]
        cmp al, byte[rsi + rcx]      		; Проверка на равные символы,
        jne .ret_false				; если символы не равны возвращаем false
        test al, al				; проверяет является ли символ конечным в обеих строках,
        je .ret_true				; если является, то возвращаем true
        inc rcx				; иначе продолжает сравнение
        jmp .loop

    .ret_false:
        xor rax, rax
        ret

    .ret_true:
        mov rax, 1
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax
    push rax		; создает буфер для символа
    xor rdi, rdi
    mov rsi, rsp	; адресс буфера
    mov rdx, 1
    syscall		; читаем
    
    cmp rax, -1	; проверяет прочитан ли символ
    je .ret_zero
    pop rax
    ret
    
    .ret_zero:
        pop rax
        xor rax, rax
        ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    push r12
    push r13
    push r14
    mov r12, rdi	; адрес начала буфера
    mov r13, rsi	; размер буффера
    xor r14, r14		; позиция курсора
    sub r13, 1
    
    .pass_spaces:
        call read_char
        test rax, rax		; проверяет есть ли слово
        jz .err		; если нет возвращает 0
        cmp rax, SPACE		; проверят символ на пробел
        je .pass_spaces	; если пробел пропускаем
        cmp rax, TAB		; проверят символ на символ табуляции
        je .pass_spaces	; если символ табуляции пропускает
        cmp rax, NEW_STRING		; проверят символ на символ переноса строки
        je .pass_spaces	; если символ переноса пропускаем
    
    .fill_buf:
    	cmp r14, r13			; проверяет !(r14 < r13) т. е. !(текущая позиция < размер буфера)
    	jnb .err			; нельзя записать символ
    	mov byte [r12 + r14], al	; записывает символ в буфер
    	inc r14			; сдвигает курсор
    	call read_char			; получает след символ
    	test rax, rax			; проверяет закончилось ли слово
    	jz .return			; если да делает возврат значений
    	cmp rax, SPACE
        je .return
        cmp rax, TAB
        je .return
        cmp rax, NEW_STRING
        je .return
        jmp .fill_buf			; иначе продолжает записывать буфер
        
    .return:
    	mov byte [r12 + r14], 0	; добавляет нуль-терминатор
    	mov rax, r12			
    	mov rdx, r14
    	jmp .final
        
    .err:
        xor rax, rax
        xor rdx, rdx
    	jmp .final
    	
    .final:
        pop r14 ; возвращает первоначальное состояние callee-saved регистров
        pop r13
        pop r12
        ret
    
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rcx, rcx	; индекс символа
    xor r8, r8		; будет находится анализируемый символ
    
    .loop:
        mov r8b, byte[rdi + rcx]
        cmp r8, MIN			; проверяет является ли символ
        jb .stop			; символом цифры
        cmp r8, MAX			; цифры в таблице ASCII с номерами 0x30 - 0x39
        ja .stop
        
        and r8b, 0xf			; превращает символ в цифру
        mov r9, 10
        mul r9				; добавляет разряд 
        add rax, r8			; rax = rax * 10 + r8
        
        inc rcx			; переходит к следующему символу
    	jmp .loop
    	
    .stop:
    	mov rdx, rcx
    	
    ret


; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor r8, r8			; проверяет начинается ли строка с минуса
    mov r8b, byte[rdi]
    cmp r8, '-'
    je .parse_neg		; если число положительное использует функцию написанную ранее
    jmp parse_uint
    
    .parse_neg:		; иначе сдвигает указатель
    	add rdi, 1
    	call parse_uint
    	neg rax		; после инвертирует значение
    	add rdx, 1
    	ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    push r12
    push r13
    push r14
    push r15
    mov r12, rdi		; указатель на строку
    mov r13, rsi		; указатель на буфер
    mov r14, rdx		; длина буфера
    call string_length		; получает длинну строки
    add rax, 1
    mov r15, rax
    cmp r15, r14		; проверяет вмещается ли строка в буфер
    ja .err
    
    
    xor rcx, rcx
    .loop:
    	cmp rcx, r15
    	je .return
    	mov dil, byte[r12 + rcx]
    	mov byte[r13 + rcx], dil
    	inc rcx
    	jmp .loop
    
    .err:
    	xor rax, rax
    	jmp .finally
    	
    .return:
    	mov rax, r15
    	
    .finally:
    	pop r15
    	pop r14
    	pop r13
    	pop r12
        ret
